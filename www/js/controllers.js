angular.module('starter.controllers', [])

.controller('ProductsController', function($scope, $http) {
  $http.get('http://localhost:8000/products')

  .then(function(response){
    $scope.products = response.data.data;

    $scope.empty = response.data.empty;
    $scope.message = response.data.message;
  });
})

.controller('ProductShowController', function($scope, $stateParams, $http) {
  $http.get('http://localhost:8000/products/' + $stateParams.code)

  .then(function(response){
    $scope.product = response.data.data;
  });
})

.controller('AboutController', function($scope) {});
