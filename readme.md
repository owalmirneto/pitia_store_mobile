# Pitia Store

This is a mobile application test for [Pitia](http://pitia.com.br) and was made with [Ionic](http://ionicframework.com)

## Installation

#### Mobile

You should be ionic installed. [Installing some dependences](http://blog.wfsneto.com.br/2015/05/06/instalando-e-configurando-gulp)

```bash
[sudo] npm install -g cordova ionic
ionic serve
```

It'll open your browser in [localhost:8100](http://localhost:8100)

See [getting started](http://ionicframework.com/getting-started) for more information

#### Main Application

You should clone, install and run [this application](https://bitbucket.org/wfsneto/pitia_store) on port 8000
